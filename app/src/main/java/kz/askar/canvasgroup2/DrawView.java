package kz.askar.canvasgroup2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;

/**
 * Created by Zhakenov on 4/8/2017.
 */

public class DrawView extends android.view.SurfaceView
        implements SurfaceHolder.Callback, View.OnTouchListener{

    boolean isRunning = false;
    SurfaceHolder surfaceHolder;
    Square square = null;
    int width = 0;
    int height = 0;

    public DrawView(Context context) {
        super(context);
        getHolder().addCallback(this);
        setOnTouchListener(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        surfaceHolder = holder;
        isRunning = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(isRunning){
                    Canvas canvas = surfaceHolder.lockCanvas();
                    if(canvas == null) continue;
                    long startTime = System.currentTimeMillis();
                    update();
                    draw(canvas);
                    long drawTime = System.currentTimeMillis() - startTime;
                    long fps = 1000/(drawTime==0?1:drawTime);
                    Log.d("fps", fps+"");
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }).start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        isRunning = false;
    }

    public void update(){
        if(square!=null){
            square.move(width, height);
        }
    }

    public void draw(Canvas canvas){
        if(width==0) width = canvas.getWidth();
        if(height==0) height = canvas.getHeight();
        if(square == null) square = new Square(0, 0, height/10, height/10);

        Paint p = new Paint();
        p.setColor(Color.RED);
        p.setStyle(Paint.Style.FILL_AND_STROKE);

        canvas.drawColor(Color.WHITE, PorterDuff.Mode.CLEAR);
        canvas.drawARGB(55, 67, 83, 127);
        canvas.drawRect(square.getRect(), p);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN){
            square.reverse();
            Log.d("touch", "down");
        }else if(event.getAction() == MotionEvent.ACTION_UP){
            square.reverse();
            Log.d("touch", "up");
        }
        return true;
    }
}
