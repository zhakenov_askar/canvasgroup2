package kz.askar.canvasgroup2;

import android.graphics.Rect;

/**
 * Created by Zhakenov on 4/8/2017.
 */

public class Square {

    int x;
    int y;
    int width;
    int height;
    int speed = 5;

    int xSign = 1;
    int ySign = 1;

    public Square(int x, int y, int width, int height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public Rect getRect(){
        return new Rect(x, y, x+width, y+height);
    }

    public void move(int screenWidth, int screenHeight){
        x+=xSign*speed;
        y+=ySign*speed;

        if(x<=0){
            x = 0;
            xSign = 1;
        }else if(x>=screenWidth - width){
            x = screenWidth - width;
            xSign = -1;
        }

        if(y<=0){
            y=0;
            ySign = 1;
        }else if(y>=screenHeight-height){
            y = screenHeight - height;
            ySign= -1;
        }
    }

    public void reverse(){
        xSign*=-1;
        ySign*=-1;
    }
}
