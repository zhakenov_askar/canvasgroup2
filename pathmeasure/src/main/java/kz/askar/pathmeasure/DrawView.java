package kz.askar.pathmeasure;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;

/**
 * Created by Zhakenov on 4/8/2017.
 */

public class DrawView extends android.view.SurfaceView
        implements SurfaceHolder.Callback{

    boolean isRunning = false;
    SurfaceHolder surfaceHolder;

    Path path;
    PathMeasure measure;
    int speed = 3;
    int currentDistance = 0;
    Matrix matrix;
    Bitmap man;

    public DrawView(Context context) {
        super(context);
        getHolder().addCallback(this);

        path = new Path();
        path.moveTo(200, 200);
        path.lineTo(400, 400);
        path.cubicTo(1200, 600, 900, 120, 1000, 1050);

        measure = new PathMeasure(path, true);

        man = BitmapFactory.decodeResource(getResources(), R.drawable.sprite);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        surfaceHolder = holder;
        isRunning = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(isRunning){
                    Canvas canvas = surfaceHolder.lockCanvas();
                    if(canvas == null) continue;
                    long startTime = System.currentTimeMillis();
                    update();
                    draw(canvas);
                    long drawTime = System.currentTimeMillis() - startTime;
                    long fps = 1000/(drawTime==0?1:drawTime);
                    Log.d("fps", fps+"");
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }).start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        isRunning = false;
    }

    public void update(){
        matrix = new Matrix();
        currentDistance+=speed;
        if(currentDistance>=measure.getLength()) currentDistance = 0;

        measure.getMatrix(currentDistance, matrix, PathMeasure.POSITION_MATRIX_FLAG
                + PathMeasure.TANGENT_MATRIX_FLAG);
    }

    public void draw(Canvas canvas){

        Paint p = new Paint();
        p.setColor(Color.RED);
        p.setStyle(Paint.Style.STROKE);
        p.setStrokeWidth(5);

        canvas.drawColor(Color.WHITE, PorterDuff.Mode.CLEAR);

//        canvas.drawCircle(1200, 600, 10, p);
//        canvas.drawCircle(900, 120, 10, p);


//        canvas.drawPath(path, p);

//        float[] pos = new float[2];
//        float[] tan = new float[2];
//
//        measure.getPosTan(600, pos, tan);
//        Log.d("velichina", "pos"+pos[0]+","+pos[1]+"; tan"+tan[0]+","+tan[1]);

        Rect rect = new Rect(-100, -50, 100, 50);
        canvas.setMatrix(matrix);
        canvas.drawRect(rect, p);


        canvas.setMatrix(new Matrix());
        canvas.drawBitmap(man, new Rect(0,0,624,450), new Rect(0,0,600,600),null);
        //game with matrix
//        Rect rect = new Rect(200, 200, 400, 400);
//        canvas.drawRect(rect, p);
//
//        p.setColor(Color.GREEN);
//        Matrix m = new Matrix();
//        m.setSkew(2, 2, 400, 400);
//        m.preTranslate(100, 100);
//        m.preRotate(30, 300, 300);
//        m.postScale(2, 2, 400, 400);
//        canvas.setMatrix(m);
//        canvas.drawRect(rect, p);
//        canvas.setMatrix(null);
//        canvas.drawRect(rect, p);
    }
}
